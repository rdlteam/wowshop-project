<?php
if(!defined("SECURITY")) 
	header($_SERVER['SERVER_PROTOCOL'] . ' 403 Forbidden', true, 403);


/* Session */
use Rdl\Session\Session,
	Zend\Db\TableGateway\TableGateway,
	Zend\Session\SaveHandler\DbTableGateway,
	Zend\Session\SaveHandler\DbTableGatewayOptions,
	Zend\Session\Config\StandardConfig,
	Zend\Session\SessionManager,
	Zend\Session\Validator\RemoteAddr,
	Zend\Session\Validator\HttpUserAgent,
	Zend\Session\Container;

/* Main */
use Zend\Config\Config,
	Rdl\Auth\Auth,
	Rdl\Route\Route,
	Rdl\Components\Loader,
	Rdl\Loader\AutoLoader,
	Rdl\Version\Version,
	Rdl\helpers\helper,
	Zend\Debug\Debug,
	Rdl\Panel\Panel;

/* Template & Helpers*/
use Zend\View\Model\ViewModel,
	Zend\View\Renderer\PhpRenderer,
	Rdl\View\Template;

/* Security */
use Rdl\Request\Request,
	IDS\Init,
	IDS\Monitor;

Final class RDLCore {
	private static $instance = false;

	public $Config = array();
	public $Url = false;

	public $Service = 'index';
	public $subTitle = '';

	public $pageTemplate = false;
	public $DefaultTemplate = false;
	public $title = false;

	public static function getInstance($Service = false){
		if(!self::$instance) 
			self::$instance = new self;

		if($Service !== false)
			self::$instance->Service = &$Service;

		return self::$instance;
	}

	function __construct(){
		
		date_default_timezone_set('Europe/Moscow');

		header('Content-Type: text/html; charset=utf-8');		
		
		require_once ENGINE_DIR. 'Core/lib/Rdl/Registry/Registry.php';

		$this->Config = new Config(require_once(ENGINE_DIR .'Core/config/global.config.php'), true);
		$this->DefaultTemplate = $this->Config->Site->Template;
		$this->Url = $this->Config->get('url', $this->Config->protocol . $_SERVER['HTTP_HOST']);

	}

	public function Run(){
		if(!$this->Service)
			throw new RuntimeException('Initialization Error service');

		$this->SecurityIDS();

		$this->ThumbsService();

		Registry::getInstance()->Helper = new PhpRenderer(); 
		Registry::getInstance()->Helper->headTitle()->setSeparator(' - ');
		Registry::getInstance()->Helper->headTitle()->append($this->Config->Site->title);

		Registry::getInstance()->DataBase = new Zend\Db\Adapter\Adapter(array(
			'driver' => $this->Config->db->driver,
    		'database' => $this->Config->db->database,
    		'username' => $this->Config->db->username,
    		'password' => $this->Config->db->password,
    		'charset'  => 'utf8',
    			'driver_options' => array(
					\PDO::MYSQL_ATTR_INIT_COMMAND => 'SET CHARACTER SET \'UTF8\'',
					\PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES UTF8'
				)
    		)
 		);

		$this->SessionStart();

		Registry::getInstance()->Auth  	  = new Auth();
		Registry::getInstance()->Auth->Run();
		
		Registry::getInstance()->SpeedBar = new \ArrayObject();

		Registry::getInstance()->Route 	  = new Route();

		if(mb_strtolower($this->Service) == 'admin')
			$this->Config->Site->Template = 'adminv3';

		Registry::getInstance()->Template = new Template();

		Loader::getInstance()->Run();
		
		switch(mb_strtolower($this->Service)){
			case 'index' :
				$this->HeaderGenerate();
				break;
			case 'admin' :
				
				if(Registry::getInstance()->Auth->isLock()){
					$this->pageTemplate = 'locked';
				} else if(!Registry::getInstance()->Auth->isLogin()){
					$this->pageTemplate = 'login';
				}

				$this->HeaderGenerate();
				break;
		}
	}

	private function SessionStart(){
		$SessionConfig = new StandardConfig();
		$SessionConfig->setOptions(array(
		   'remember_me_seconds' => 2419200,
		   'cookie_lifetime'     => 2419200,
		   'use_cookies'		 => true,
		   'cookie_secure'		 => true,
		   'cookie_httponly'	 => true,
		   'use_trans_sid'		 => false,
		   'name'                => 'rdl'
		   )
		);

		Registry::getInstance()->Session  = new SessionManager($SessionConfig);
		if($this->Config->Session->Storage == 'base'){
			$tableGateway = new TableGateway('rdl_session', Registry::getInstance()->DataBase);
			$saveHandler  = new DbTableGateway($tableGateway, new DbTableGatewayOptions());
			Registry::getInstance()->Session->setSaveHandler($saveHandler);
		}

		if($this->Config->Session->Validate == 'remoteaddr')
			Registry::getInstance()->Session->getValidatorChain()->attach('session.validate', array(new RemoteAddr(), 'isValid'));
		else if($this->Config->Session->Validate == 'httpuseragent')
			Registry::getInstance()->Session->getValidatorChain()->attach('session.validate', array(new HttpUserAgent(), 'isValid'));

		Registry::getInstance()->Session->start();
		Registry::getInstance()->SessionStorage = new Container('SessionData', Registry::getInstance()->Session);
		if (!isset(Registry::getInstance()->Session->init) && $this->Config->Session->Remember == true) {
             Registry::getInstance()->Session->regenerateId(true);
             Registry::getInstance()->SessionStorage->init = 1;
        }
	}
	private function HeaderGenerate(){
		Registry::getInstance()->Template->Set('content', Registry::getInstance()->Template->Content);

		Registry::getInstance()->Helper->headMeta()->appendHttpEquiv('Content-Type', 'text/html; charset='. strtoupper($this->Config->charset));
		if(count($this->Config->Site->meta) > 0) foreach($this->Config->Site->meta as $Name => $Value){
				Registry::getInstance()->Helper->headMeta()->appendName($Name, $Value);
			}
	
		Registry::getInstance()->Template->Set('Header', Registry::getInstance()->Helper->headMeta() . "\n");
		Registry::getInstance()->Template->Set('Title',  Registry::getInstance()->Helper->headTitle() . "\n");
		Registry::getInstance()->Template->Display($this->pageTemplate);
		Registry::getInstance()->Template->Clear();
	}

	/*
	* Подключение фаервола phpIDS
	*/
	private function SecurityIDS(){
		
		$arRequest = array(
			'REQUEST' => Request::GetParam(array('method' => 'REQUEST')),
			'GET' 	  => Request::GetParam(array('method' => 'GET')),
			'POST' 	  => Request::GetParam(array('method' => 'POST')),
			'COOKIE'  => $_COOKIE
		);

		$obIDS = Init::init(__DIR__ . '/lib/IDS/Config/Config.ini');
  		$obIDS->config['General']['base_path'] = __DIR__ . '/lib/IDS/';
		$obIDS->config['General']['use_base_path'] = true;
		$obIDS->config['General']['exceptions'][] = 'GET.thumb_file';
		$obIDS->config['General']['exceptions'][] = 'COOKIE.SpryMedia_DataTables_table-1_admincatalogorders';
		
		if($this->Service === 'admin'){
			$obIDS->config['General']['exceptions'][] = 'POST.page';
		}

		if(isset($_COOKIE['rdl_user_hash'], $_COOKIE['rdl_user_id'])){
			$obIDS->config['General']['exceptions'][] = 'COOKIE.rdl_user_hash';
		}

  		Registry::getInstance()->Security = new Monitor($obIDS);
		if (!Registry::getInstance()->Security->run($arRequest)->isEmpty()) 
			throw new Rdl\Exception\HackedPage('Попытка взлома');
		
	}

	private function ThumbsService(){
		if($this->Service != 'thumbs')
			return false;

		$thumb = Bazalt\Thumbs\Image::generateThumb(ROOT_DIR . Request::Get('thumb_file'), new Bazalt\Thumbs\Operations());
		if ($thumb) {
		    switch (pathinfo($thumb, PATHINFO_EXTENSION)) {
		    case 'png':
		        header('Content-Type: image/png');
		        break;
		    case 'jpg':
		        header('Content-Type: image/jpeg');
		        break; 
		    }
		    readfile($thumb);
		    exit;
		}
		die();
	}

	public function isAjax(){
		return (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH'], 'UTF-8') == 'xmlhttprequest');
	}
}