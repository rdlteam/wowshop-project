<?php
namespace Rdl\Auth;

use \Zend\Db\TableGateway\TableGateway;
use \Zend\Db\TableGateway\Feature\RowGatewayFeature;
use \Rdl\Request\Request;
use \Zend\Authentication\Adapter\DbTable as AuthAdapter;
use \Zend\Authentication\AuthenticationService;
use \Zend\Authentication\Storage\Session;
use \Zend\Crypt\Password\Bcrypt;
use \Rdl\Security\Security;
use \Rdl\Exception\AjaxException;

class Auth extends \Rdl\CoreFactory {
	
	protected $Auth = false;
	protected $obAuth = false;
	protected $obUser = false;
	protected $obCrypt = false;

	protected $CookieUserID = 'rdl_user_id';
	protected $CookieUserHash = 'rdl_user_hash';

	public function __construct(){
		parent::__construct();
		$this->obCrypt = new Bcrypt(array('salt' => $this->Core->Config->HashKey, 'cost' => 13));
	}

	public function Run(){
		
		if(Request::Get('logout') == 'Y') $this->Logout();
		if(Request::Get('lock') == 'Y') $this->Lock();

		if($this->isLogin() && !$this->isLock()){
			$this->obUser = new TableGateway('rdl_users', $this->Registry->DataBase);
			$this->obUser = $this->obUser->Select('id = '.(int) $this->Registry->SessionStorage->User['user_id'])->current();
			if(!$this->obCrypt->verify($this->obUser->id . $this->obUser->password, $this->Registry->SessionStorage->User['user_hash']))
				$this->Logout();
		}

		if(Request::Post('user_login') AND Request::Post('user_password')){			
			
			Security::getInstance()->verifyOrFail();
			$this->Authorize();

		} else if($this->isLock() && Request::Post('user_password')){
			
			Security::getInstance()->verifyOrFail();
			$this->Authorize();

		} else if(isset($_COOKIE['rdl_user_id'], $_COOKIE['rdl_user_hash']) && !$this->isLogin()){
			if($this->Core->Config->remember == true)
				$this->Authorize(true);
			else
				$this->Logout();
		}
	}

	private function Authorize($remember = false){
		$this->Auth = new AuthAdapter($this->Registry->DataBase, 'rdl_users', 'auth_login', 'password', 'MD5(?)');
		
		$authType = $this->Core->Config->AuthType;
		
		if($this->isLock())
			$authType = 35;

		if($remember == true)
			$authType = 2;

		switch((int) $authType){
			case 1:
				// Авторизация в качестве логина "Email"
				$this->Auth->setIdentityColumn('email');
				$this->Auth->setIdentity(Request::Post('user_login'))->setCredential(Request::Post('user_password'));
				break;
			case 2:	
				// Восстановление авторизации "Запомнить меня"
				$this->Auth->setIdentityColumn('id')->setCredentialColumn('hash');
				$this->Auth->setIdentity((int) $_COOKIE['rdl_user_id'])->setCredential($_COOKIE['rdl_user_hash']);
				break;
			case 35:
				// Восстановление авторизации функция "Заблокировать"
				$this->Auth->setIdentityColumn('id');
				$this->Auth->setIdentity($this->Registry->SessionStorage->User['user_id'])->setCredential(Request::Post('user_password'));
				break;
			default:
				// Обычная авторизация
				$this->Auth->setIdentity(Request::Post('user_login'))->setCredential(Request::Post('user_password'));
				break;
		}

		$this->obAuth = new AuthenticationService;
		$result = $this->obAuth->authenticate($this->Auth);
		
		if(!$result->isValid()){
			
			if($this->Core->Service == 'ajax')
				throw new AjaxException(13001, 'Сочетание логин и пароль не найдено');
			else {
				$this->arError['Code'] = 13001;
				$this->arError['Message'] = 'Сочитания логин и пароль не найдено';
			}

		} else {

			if(!$this->isLock()){
        		$identity = $this->Auth->getResultRowObject();
        		$this->Registry->SessionStorage->Auth = true;
        		$this->Registry->SessionStorage->User = array(
        			'user_id' => $identity->id, 
        			'user_hash' => $this->obCrypt->create($identity->id . $identity->password),
        			'user_login' => $identity->login
        		);
        		
        		$this->RememberMe();
        		
        	} else unset($this->Registry->SessionStorage->Lock);

        	if($this->Core->Service == 'ajax')
        		die(json_encode(array('Error' => false)));

			if($this->Core->Service != 'ajax' && Request::Post('request_page')){
        		header('Location: '. Request::Post('request_page'));
        		die();
        	}
        }
	}

	private function RememberMe(){

		if($this->Core->Config->remember != true){ 
			$this->ClearRemember();
			return false;
		}

		$obUser = new TableGateway('rdl_users', $this->Registry->DataBase, new RowGatewayFeature('id'));
		$arUser = $obUser->select('id = '.(int) $this->Registry->SessionStorage->User['user_id'])->current();
		$arUser->hash = md5(sha1($this->Registry->SessionStorage->User['user_hash']));
		$arUser->save();

		$domain = ($_SERVER['HTTP_HOST'] != 'localhost') ? $_SERVER['HTTP_HOST'] : false;
		setcookie($this->CookieUserID, $this->Registry->SessionStorage->User['user_id'], time()+60*60*24*365, '/', $domain, false);
		setcookie($this->CookieUserHash, sha1($this->Registry->SessionStorage->User['user_hash']), time()+60*60*24*365, '/', $domain, false);		
	}

	public function ClearRemember(){

		$domain = ($_SERVER['HTTP_HOST'] != 'localhost') ? $_SERVER['HTTP_HOST'] : false;
		if(isset($_COOKIE[$this->CookieUserID]))
			setcookie($this->CookieUserID, NULL, time()+60*60*24*365, '/', $domain, false);

		if(isset($_COOKIE[$this->CookieUserHash]))
			setcookie($this->CookieUserHash, NULL, time()+60*60*24*365, '/', $domain, false);

	}

	public function Logout(){
			if($this->Core->Service != 'index' && !$this->isLogin())
				return false;
			
			unset($this->Registry->SessionStorage->Auth);
			unset($this->Registry->SessionStorage->User);
			if(isset($this->Registry->SessionStorage->Lock))
				unset($this->Registry->SessionStorage->Lock);

			$this->ClearRemember();

			$Location = '/';
			if(Request::Get('return'))
				$Location .= Request::Get('return');

			header('Location: '. $Location);
			die();
	}

	public function Lock(){
		$this->Registry->SessionStorage->Lock = true;
		header('Location: /admin/');
		die();
	}

	public function __get($name){
		
		if(!$this->isLogin())
			return false;

		return isset($this->obUser[$name]) ? $this->obUser[$name] : false;
	}
	public function isLogin(){
		return (isset($this->Registry->SessionStorage->Auth) && $this->Registry->SessionStorage->Auth === true);
	}

	public function isLock(){
		return (isset($this->Registry->SessionStorage->Lock) && $this->Registry->SessionStorage->Lock === true);
	}
	public function isAdmin(){
		return isset($this->obUser) && $this->obUser['login'] == 'max';
	}
}