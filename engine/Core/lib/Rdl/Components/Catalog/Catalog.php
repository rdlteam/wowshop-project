<?php
namespace Rdl\Components\Catalog;

use Zend\Cache\StorageFactory;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\TableGateway\Feature\RowGatewayFeature;
use Zend\Db\Sql\Select;
use Rdl\Request\Request;

class Catalog extends \Rdl\CoreFactory {
	protected $Cache = false;
	protected $arReturn = array();
	public $CacheID = 'CatalogCategoryCache';

	public function __construct(){
		parent::__construct();
		if(!$this->Cache)
			$this->Cache = StorageFactory::factory(
				array(
					'adapter' => array(
						'name' => 'filesystem',
						'options' => array('cache_dir' => ENGINE_DIR .'cache/'),
					),
				)
			);	

		if(Request::Get('clear_cache') == 'Y')
			$this->CacheClear();

		$this->arReturn = json_decode($this->Cache->getItem($this->CacheID), true);
	}

	public function CacheRun(){
		if(!$this->Cache->getItem($this->CacheID)){
			$obQuery = new TableGateway('rdl_catalog_category', $this->Registry->DataBase);
			$obCategory = $obQuery->select(function (Select $select) {
     				$select->order('parent_category ASC');
				});
			$arData = array();
			foreach($obCategory->toArray() as $arCategory){
				$this->arReturn[$arCategory['id']] = $this->arReturn['alt'][$arCategory['altname']] = $arCategory;
				if($arCategory['parent_category'] > 0){
					$this->arReturn['parent_category'][$arCategory['parent_category']][$arCategory['id']] = $this->arReturn['alt'][$arCategory['parent_category']][$arCategory['altname']] = &$this->arReturn[$arCategory['id']];
				}
			}
	
			$this->Cache->setItem($this->CacheID, json_encode($this->arReturn));
		}
	}

	public function getCategoryId($id){
		if(intval($id) <= 0)
			return false;

		return isset($this->arReturn[$id]) ? $this->arReturn[$id] : false;
	}

	public function getCategoryName($name, $parent_category = false){
		$name = filter_var($name, FILTER_SANITIZE_STRING);
		if(mb_strlen($name) <= 0)
			return false;
		
		if($parent_category !== false)
			return isset($this->arReturn['alt'][$parent_category]['id'], $this->arReturn['alt'][$this->arReturn['alt'][$parent_category]['id']][$name]) ? $this->arReturn['alt'][$this->arReturn['alt'][$parent_category]['id']][$name] : false;
		else
			return isset($this->arReturn['alt'][$name]) ? $this->arReturn['alt'][$name] : false;
	}

	public function getCategorys($category){
		$name = filter_var($category, FILTER_SANITIZE_STRING);
		if(mb_strlen($name) <= 0 OR empty($this->arReturn['alt'][$name]['id']))
			return false;

		return isset($this->arReturn['alt'][$name]['id'], $this->arReturn['parent_category'][$this->arReturn['alt'][$name]['id']]) ? $this->arReturn['parent_category'][$this->arReturn['alt'][$name]['id']] : false;
	}

	public function GetCategoryList($category = ''){
		if(empty($category)){
			$arCategory = $this->arReturn;
			unset($arCategory['alt'], $arCategory['parent_category']);
			return $arCategory;
		}

		$category = filter_var($category, FILTER_SANITIZE_STRING);
		if(isset($this->arReturn[$category]))
			return $this->arReturn[$category];
		elseif(isset($this->arReturn['alt'][$category]))
			return $this->arReturn['alt'][$category];
		else
			return array();
	}

	public function CacheClear(){
		return $this->Cache->removeItem($this->CacheID);
	}


}