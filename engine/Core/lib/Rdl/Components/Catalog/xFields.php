<?php
namespace Rdl\Components\Catalog;

use Zend\Cache\StorageFactory;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\TableGateway\Feature\RowGatewayFeature;
use Zend\Db\Sql\Select;
use Rdl\Request\Request;

class xFields extends \Rdl\CoreFactory {
	protected $Cache = false;
	protected $arReturn = array();
	protected $arFields = array();

	public $CacheIDFields = 'xFieldCache';

	public function __construct(){
		parent::__construct();
		if(!$this->Cache)
			$this->Cache = StorageFactory::factory(
				array(
					'adapter' => array(
						'name' => 'filesystem',
						'options' => array('cache_dir' => ENGINE_DIR .'cache/'),
					),
				)
			);	

		if(Request::Get('clear_cache') == 'Y')
			$this->CacheClear();
	}

	public function RunCacheFields(){
		$this->arFields = json_decode($this->Cache->getItem($this->CacheIDFields), true);
		if(!$this->Cache->getItem($this->CacheIDFields)){
			$obQuery = new TableGateway('rdl_catalog_xfields', $this->Registry->DataBase);
			foreach($obQuery->select(array())->toArray() as $arFields){
				$this->arFields[$arFields['alt_name']] = $arFields;
			}

			$this->Cache->setItem($this->CacheIDFields, json_encode($this->arFields));
		}
	}

	public function getFieldName($field){
		return isset($this->arFields[$field]) ? $this->arFields[$field]['name'] : false;
	}

	public function getValueField($field = false, $product_id = 0){
		if(!$field || intval($product_id) <= 0)
			return false;

		if(!is_array($field) && !isset($this->arFields[$field]))
			return false;

		$obValueFields = new TableGateway('rdl_catalog_xfields_value', $this->Registry->DataBase);
		$obValue = $obValueFields->Select(array('xfield_id' => (int) $this->arFields[$field]['id'], 'product_id' => (int) $product_id));

		switch(mb_strtolower($this->arFields[$field]['type'], $this->Core->Config->charset)){
			case "multiple":
				$arReturn = array();
				
				foreach($obValue->toArray() as $row){
					$arReturn[] = $row;
				}

				return $arReturn;
				break;
			default:
				return $obValue->Current();
		}

	}

	public function getFieldsProduct($product_id = 0){
		if(intval($product_id) <= 0)
			return false;

		$obValueFields = new TableGateway('rdl_catalog_xfields_value', $this->Registry->DataBase);
		$obValue = $obValueFields->Select(function(Select $select) use($product_id){
			$select->join(array('fields' => 'rdl_catalog_xfields'), 'rdl_catalog_xfields_value.xfield_id = fields.id');
			$select->where('rdl_catalog_xfields_value.product_id = '.(int) $product_id);
		});

		$arReturn = array();
		foreach($obValue->toArray() as $arFields){
			unset($arFields['name'], $arFields['default']);
			if($arFields['type'] == 'multiple'){
				unset($arFields['type']);
				$arReturn[$arFields['alt_name']][] = $arFields['value'];
			} else {
				unset($arFields['type']);
				$arReturn[$arFields['alt_name']] = $arFields['value'];
			}
		}
		return $arReturn;
	}

	public function CacheClear(){
		return $this->Cache->removeItem($this->CacheIDFields);
	}


}