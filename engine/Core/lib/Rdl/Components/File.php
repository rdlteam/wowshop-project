<?php
namespace Rdl\components;
use Bazalt\Thumbs\Image;

class File extends \Rdl\CoreFactory {
	protected $arFiles = array();
	
	protected function GetFile($file_id = false){
		if($file_id === false)
			return false;

		$_where = ' `id` = ' . (int) $file_id;
		if(is_array($file_id)){
			$arFileID = array();
			foreach($file_id as $key => $fileID){
				if(intval($fileID) <= 0) continue;
				$arFileID[] = (int) $fileID;
			}
			if(count($arFileID) > 0)
				$_where = ' `id` IN (' . join(', ', $arFileID) .')';

			unset($arFileID);
		}

		$obQueryFiles = $this->Registry->DataBase->Query('SELECT * FROM `rdl_files` WHERE ' . $_where)->execute();
		unset($_where);
		if($obQueryFiles->Count() <= 0)
			return false;

		$arReturn = array();
		while($arFile = $obQueryFiles->Current()){
			$arReturn[$arFile['id']] = $arFile;
		}

		return $arReturn;
	}

	public function getImages($images_id, $thumbs = false){
		// die('test');
		if(empty($images_id))
			return false;

		$arFiles = $this->GetFile($images_id);
		if(empty($arFiles))
			return false;

		if($thumbs === false) 
			return $arFiles;

		Image::initStorage(ROOT_DIR . '/upload/image_cache', $this->Core->Url . '/index.php?thumb_file=/upload/image_cache');
		$arReturn = array();
		foreach($arFiles as $fileID => $arFile){
				$arReturn[$fileID] = array_merge($arFile, array('thumbs_url' => Image::getThumb(ROOT_DIR . $arFile['path'], $thumbs, [])));
		}
		unset($arFiles);
		return $arReturn;
	}

	public function ResizeImage($path, $image){
		Image::initStorage(ROOT_DIR . '/upload/image_cache', $this->Core->Url . '/index.php?thumb_file=/upload/image_cache');
		return Image::getThumb(ROOT_DIR . $path, $image, []);
	}	
}