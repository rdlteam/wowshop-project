<?php
namespace Rdl\components;

use Zend\Cache\StorageFactory;
use Rdl\Route\Route;
use Rdl\Module\Standart;
use Rdl\Exception\ErrorPage;
use \FileSystemIterator;
use \RecursiveIteratorIterator;
use Rdl\Request\Request;
use Zend\Db\TableGateway\TableGateway;

class Loader extends \Rdl\CoreFactory {

	private static $instance = false;
	
	public $arPages = false;
	public $arPagesQuery = false;
	public $arAlternativePages = false;

	private $arControllers = false;

	public $PageParams = array();
	private $Cache = false;

	static public function getInstance(){
		if(!self::$instance)
			self::$instance = new self;


		return self::$instance;
	}

	function __construct(){
		parent::__construct();
		$this->Cache = StorageFactory::factory(
			array(
				'adapter' => array(
					'name' => 'filesystem',
					'options' => array('cache_dir' => ENGINE_DIR .'cache/'),
				),
			)
		);
	}

	public function Run(){
		$this->LoadingRouteRule();

		$this->LoadingPage($this->Core->Service);
		$this->LoadingControllerBase();

		$this->Registry->Route->Run();


		if($this->Core->Service == 'ajax'){
			$page = array();
			
			if($GetController = Request::Get('controller'))
				$page['controller'] = $GetController;
			
			if($GetActionController = Request::Get('action'))
				$page['action']		= $GetActionController;

			$this->LoaderController($page);
		} else {	
			if(!isset($this->arPages[$this->Registry->Route->Page]))
				throw new ErrorPage(404);

			$this->LoaderController($this->arPages[$this->Registry->Route->Page]);
			$this->Core->pageTemplate = $this->arPages[$this->Registry->Route->Page]['template'];
		}
	}

	public function LoadingPage($service){
		
		$cacheID = 'pages-'.$service;

		$this->arPages = json_decode($this->Cache->getItem($cacheID), true);
		
		if(!$this->Cache->getItem($cacheID)){
			$obQuery = new TableGateway('rdl_pages', $this->Registry->DataBase);
			$arPagesStack = array();
			foreach($obQuery->select(array('service' => mb_strtolower($service)))->toArray() as $arPage){
				
				if($arPage['parent'] > 0 && !empty($arPagesStack[$arPage['parent']])){
					$arPage['url'] = $arPagesStack[$arPage['parent']]['url'] . $arPage['url'];
				}
	
				$arPagesStack[$arPage['id']] = $arPage;
	
				if(strpos(trim($arPage['url']), '.php') === false && strpos(trim($arPage['url']), '.html') === false){
					$this->arAlternativePages[sha1(trim($arPage['url'] . 'index.php'))] = $this->arAlternativePages[sha1(trim($arPage['url'] . 'index.html'))] = &$this->arPagesQuery[sha1(trim($arPage['url']))];
				}
	
				$this->arPagesQuery[sha1(trim($arPage['url']))] = $arPage;
			}
	
			$this->arPages = array_merge($this->arPagesQuery, $this->arAlternativePages);
			unset($arPagesStack);
	
			$this->Cache->setItem($cacheID, json_encode($this->arPages));
		} 
		

		if(is_array($this->arPages))
			$this->Registry->Route->arPages = array_keys($this->arPages);
	}
	
	public function ClearCache($cache){
		return $this->Cache->removeItem($cache);
	}

	private function LoadingControllerBase(){
		$obQuery = new TableGateway('rdl_controllers', $this->Registry->DataBase);
		foreach($obQuery->select()->toArray() as $arController){
			$this->arControllers[ucfirst($arController['prefix'])][$arController['name']] = $arController;
		}
	}
	
	private function LoadingRouteRule(){
		foreach(new \Rdl\Loader\SplRecursive(ENGINE_DIR .'components/', 'php') as $entry) {
	    	$this->Registry->Route->RouteRule = array_merge($this->Core->Route->RouteRule, include_once($entry->getRealPath()));
		}
	}

	private function LoaderController(array $page = array()){
		// var_dump($page);
		// die();

		$this->Registry->Template->pageTitle = isset($page['title']) ? $page['title'] : '';
		if(empty($page['controller'])){

			$this->Registry->Template->title[] = isset($page['title']) ? $page['title'] : '';

			$this->Registry->Helper->headTitle()->append(isset($page['title']) ? $page['title'] : '');
			$this->Registry->SpeedBar[(isset($page['url']) ? $page['url'] : '')] = isset($page['title']) ? $page['title'] : '';
			$this->Registry->Template->pageTitle = isset($page['title']) ? $page['title'] : '';
			$this->Registry->Template->Content .= isset($page['page']) ? $page['page'] : '';

		} else {

			$controller = explode(':', $page['controller'], 2);
			if(!isset($this->arControllers[ucfirst($controller[0])][$controller[1]]))
				throw new \Rdl\Exception\ErrorPage('301');
			elseif($this->arControllers[ucfirst($controller[0])][$controller[1]]['active'] === false)
				throw new \Rdl\Exception\ErrorPage('301');

			$serviceController = 'controller';
			
			if($this->Core->Service == 'admin' && ucfirst($controller[0]) != 'Admin')
				$serviceController = 'Admin' . ucfirst($serviceController);

			$Path = ENGINE_DIR .'/components/'. ucfirst($controller[0]) .'/'. $controller[1] .'/'.$serviceController.'.php';
			
			if(!file_exists($Path))
				throw new \Rdl\Exception\ErrorPage('301');

			require_once $Path;
			$className = ucfirst($controller[1]) .'Controller';

			if(!class_exists($className))
				throw new \Rdl\Exception\ErrorPage('301');

			$Class = new $className;
			$Class->getTypeController(ucfirst($controller[0]));

			if(method_exists($className, 'Options'))
				$Class->Options();

			if(empty($page['action']))
				$page['action'] = 'index';

			if($this->Core->Service == 'ajax')
				$page['action'] = $page['action'].'Ajax';
			
			$page['action'] = ucfirst($page['action']);

			
			if(isset($this->Registry->Route->Params['Action']))
				$page['action'] = $this->Registry->Route->Params['Action'];

			$Action = $page['action'] . 'Action';
			$BeforeAction = 'Before'. $page['action'];
			$AfterAction  = 'After'. $page['action'];

			if(!method_exists($className, $Action))
				throw new \Rdl\Exception\ErrorPage('301');
			
			if(isset($page['arParams']))
				$Class->getParams(json_decode($page['arParams']));
			
			ob_start();
			if(method_exists($className, $BeforeAction))
				$Class->{$BeforeAction}();

			echo $Class->{$Action}();

			if(method_exists($className, $AfterAction))
				$Class->{$AfterAction}();

			$this->Registry->Template->Content .= ob_get_contents();
			ob_end_clean();
		}
	}
}