<?php
namespace Rdl\components;

use Zend\Cache\StorageFactory;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\TableGateway\Feature\RowGatewayFeature;
use Zend\Db\Sql\Select;

class Menu extends \Rdl\CoreFactory {
	protected $Cache = false;
	protected $arReturn = array();
	public $CacheID = 'MenuCategoryCache';

	public function __construct(){
		parent::__construct();
		if(!$this->Cache)
			$this->Cache = StorageFactory::factory(
				array(
					'adapter' => array(
						'name' => 'filesystem',
						'options' => array('cache_dir' => ENGINE_DIR .'cache/'),
					),
				)
			);
	}

	public function CacheRun(){
		// $this->arReturn = json_decode($this->Cache->getItem($this->CacheID), true);
		// if(!$this->Cache->getItem($this->CacheID)){
		// 	$obQuery = new TableGateway('rdl_catalog_category', $this->Registry->DataBase);
		// 	$arData = array();
		// 	foreach($obQuery->select(array())->toArray() as $arCategory){
		// 		$this->arReturn[$arCategory['id']] = $this->arReturn['alt'][$arCategory['altname']] = $arCategory;
		// 		if($arCategory['parent_category'] > 0)
		// 			$this->arReturn['parent_category'][$arCategory['parent_category']][$arCategory['id']] = &$this->arReturn[$arCategory['id']];
		// 	}
	
		// 	$this->Cache->setItem($this->CacheID, json_encode($this->arReturn));
		// }
		$CacheID = 'Parent'.$this->CacheID;
		$this->arReturn['parent'] = json_decode($this->Cache->getItem($CacheID), true);
		if(empty($this->arReturn['parent'])){
			$obQuery = new TableGateway('rdl_menu', $this->Registry->DataBase);
			$arData = array();
			foreach($obQuery->Select(array())->toArray() as $arMenu){
				$this->arReturn['parent'][$arMenu['id']] = $this->arReturn['parent'][$arMenu['altername']] = $arMenu;
			}
	
			$this->Cache->setItem($CacheID, json_encode($this->arReturn['parent']));
		}

		$CacheID = 'Children'.$this->CacheID;
		$this->arReturn['menu'] = json_decode($this->Cache->getItem($CacheID), true);
		if(empty($this->arReturn['menu'])){
			$obQuery = new TableGateway('rdl_menu_line', $this->Registry->DataBase);
			$arData = array();
			foreach($obQuery->Select(array())->toArray() as $arMenu){
				$this->arReturn['menu'][$arMenu['menu_id']][$arMenu['id']] = $arMenu;
			}
	
			$this->Cache->setItem($CacheID, json_encode($this->arReturn['menu']));
		}
	}

	public function GetMenu($altername = false){
		if(empty($altername))
			return false;

		$altername = filter_var($altername, FILTER_SANITIZE_STRING);

		$arMenu['menu'] = $this->arReturn['parent'][$altername];
		$arMenu['line'] = isset($this->arReturn['menu'][$arMenu['menu']['id']]) ? $this->arReturn['menu'][$arMenu['menu']['id']] : false;

		return $arMenu;
	}

	public function CacheClear(){
		$this->Cache->removeItem('Parent'.$this->CacheID);
		return $this->Cache->removeItem('menu'.$this->CacheID);
	}


}