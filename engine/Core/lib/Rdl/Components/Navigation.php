<?php
namespace Rdl\Components;

use \Rdl\Request\Request;

Class Navigation extends \Rdl\CoreFactory {

    public $thisPage = 1;
    public $Prefix = 'page';
    public $allPages = 0;
    public $colPage = 1;

    public function Query($Query = false){
        if(!$Query) return false;
        $this->thisPage = Request::Get($this->Prefix) ? Request::Get($this->Prefix, 'int') : $this->thisPage;

        $Query = $this->Registry->DataBase->Query($this->Replace($Query));
        $QueryCount = $this->Registry->DataBase->GetRow($this->Registry->DataBase->Query("SELECT FOUND_ROWS() as count"));
        
        if($this->colPage > 0)
            $this->allPages = ceil($QueryCount['count'] / $this->colPage);

        return $Query;
    }

    private function Replace($Query = false){
        if(!$Query) return false;
        $arReturn = array();
        $arReturn[] = preg_replace("/^SELECT\s+/i", "SELECT SQL_CALC_FOUND_ROWS ", $Query);
        if($this->colPage > 0)
            $Limit = ($this->thisPage - 1) * $this->colPage;
        $arReturn[] = 'LIMIT '. $Limit .','. $this->colPage;

        return join(' ', $arReturn);
    }

    public function GenerateTemplate($return = false, $template = 'navigation'){
        $arResult = array("PAGES" => array(), 'PREV' => false, 'NEXT' => false);
        if($this->thisPage < $this->allPages) $arResult['NEXT'] = $this->thisPage + 1;

        for($i = 1; $i <= $this->allPages; $i++)
            $arResult['PAGES'][] = array('page' => $i, 'current' => ($i == $this->thisPage ? true : false));
        
        if($this->thisPage > 1) $arResult['PREV'] = $this->thisPage - 1;

        $this->Registry->Template->Set('arNavigation', $arResult);
        $this->Registry->Template->Display($template, $return);
        $this->Registry->Template->Clear();
    }
}

/*class Navigation{

    public  $prefix = 'page',
            $str_page = 1,
            $str_padding = 1,
            $str_seperator = ' ',
            $tquery = false,
            $type = false,
            $param = false,
            $other = false,
            $url = array('module' => 'news');
    private $all_pages = 0,
            $this_pages = 1,
            $zquery = false,
            $Page = 1;

    function __construct(){}

    public function Query($query = false)
    {
        if(!$query)
            return false;
        if(!$this->param)
            $this->Page = Request::Get($this->prefix) ? intval(Request::Get($this->prefix)) : 1;
        else
            $this->Page = ($this->type == $this->prefix) ? intval($this->param) : 1;
            
        $this->zquery = Core::getInstance()->Getc('DataBase')->Query($this->SelectReplace($query));
        $this->tquery = array_pop(Core::getInstance()->Getc('DataBase')->GetRow(Core::getInstance()->Getc('DataBase')->Query("SELECT FOUND_ROWS()")));
        if($this->str_page != 0)
            $this->all_pages = ceil($this->tquery / $this->str_page);
        return $this->zquery;
    }

    private function SelectReplace($query = false)
    {
        if(!$query)
            return false;
        $query = preg_replace("/^SELECT\s+/i", "SELECT SQL_CALC_FOUND_ROWS ", $query);
        if($this->str_page != 0)
            $Low = ($this->Page - 1) * $this->str_page;
        return $query." LIMIT ".$Low.",".$this->str_page;
    }

    public function ShowPages()
    {
        if($this->all_pages <= 1)
            return false;
        $pages = array();
        $start = $this->Page - $this->str_padding;
        if($start < 1)
            $start = 1;
        $end = $this->Page + $this->str_padding - 1;
        if($end > $this->all_pages)
            $end = $this->all_pages;
        if($start > 1)
            $pages[] = $this->link($start - 1, $start - 2 > 0 ? '...' : '');
        for($i = $start; $i <= $end; $i++)
            $pages[] = $this->link($i);
        if($end + 1 < $this->all_pages)
            $pages[] = $this->link($end + 1, $end + 2 == $this->all_pages ? '' : '...');
        if($end + 1 <= $this->all_pages)
            $pages[] = $this->link($this->all_pages);
        return implode($this->str_seperator, $pages);
    }

    private function link($id, $name = false)
    {
        if(!$name)
            $name = $id;
            
        if($id != $this->Page){
            if(!$this->other)
                $so = array($this->prefix, $id);
            else
                $so = array($this->other, $this->prefix, $id);
            return '<a href="'.Core::getInstance()->Getc('Url')->Construct(array_merge($this->url,$so)).'" class="page">'.$name.'</a>';
        }
        return '<span class="current">'.$name.'</span>';
    }

    public function NavLink($type)
    {
        if($type == 'next')
            return ($this->Page < $this->all_pages) ? $this->Page + 1 : false;
        elseif($type == 'prev')
            return ($this->Page > 1) ? $this->Page - 1 : false;
    }

}*/