<?php

/*
	Абстрактный класс, наследуется всеми компонентами библиотеки Rdl.
	Хранит в себе синглотон вызов ядра и хранилища классов
*/

namespace Rdl;
abstract class CoreFactory{
	
	protected $Core;
	protected $Registry;

	function __construct(){
		$this->Core = \RDLCore::getInstance();
		$this->Registry = \Registry::getInstance();
	}

	public function CLoad($Controller = '', $tAction = '', $arParams = array()){
		if(empty($Controller))
			return false;
		$thisController = explode(':', $Controller, 2);
		$Path = ENGINE_DIR .'components'. DIRECTORY_SEPARATOR .ucfirst($thisController[0]). DIRECTORY_SEPARATOR .$thisController[1]. DIRECTORY_SEPARATOR .'controller.php';
		if(!file_exists($Path))
			return false;

		require_once($Path);
		$ControllerClass = ucfirst($thisController[1]).'Controller';

		if(!class_exists($ControllerClass))
			return false;
		$Class = new $ControllerClass();

		$Class->getTypeController(ucfirst($thisController[0]));
		if(method_exists($ControllerClass, 'Options'))
			$Class->Options();

		if(empty($tAction))
			$tAction = 'index';

		$tAction = ucfirst($tAction);
		
		$Action = $tAction .'Action';
		$BeforeAction = 'Before'. $tAction;
		$AfterAction  = 'After'. $tAction;

		if(!method_exists($ControllerClass, $Action))
			return false;
		
		if(count($arParams) > 0)
			$Class->getParams($arParams);
		
		if(method_exists($ControllerClass, $BeforeAction))
			$Class->{$BeforeAction}();
		
		echo $Class->{$Action}();

		if(method_exists($ControllerClass, $AfterAction))
			$Class->{$AfterAction}();
	}
}