<?php
namespace Rdl\Db;

class RdlOrm extends \Rdl\CoreFactory {
	
	private static $instance = false;

	private static $arQuery = array("SELECT" 	=> '', 
									"DELETE" 	=> '', 
									"UPDATE" 	=> '',
									"INSERT INTO" => '',
									"FROM" 		=> '', 
									"SET" 		=> '', 
									"WHERE" 	=> '',
									"GROUP" 	=> '',
									"ORDER BY" 	=> '',
									"LIMIT" 	=> '');
	
	private static $SuccessQuery = false;
	private static $CountCNT = false;

	private static $arData = array();

	public static function Get($Table = false){
		if(!$Table)
			return false;

		self::$arQuery['FROM'] = '`'. filter_var($Table, FILTER_SANITIZE_STRING) . '`';

		if(!self::$instance)
			self::$instance = new self;

		self::$SuccessQuery = false;

		return self::$instance;
	}

	public function Select($arSelect = false){
		if(!$arSelect){
			if(count(self::$arData) <= 0)
				return false;

			$arSelect = array_keys(self::$arData);
			self::$arData = array();
		}

		$arSelectOrm = array();
		if(is_array($arSelect)){ 
			foreach($arSelect as $Param){
				$arSelectOrm[] = $this->Select($Param);
			}
			self::$arQuery['SELECT'] = join(', ', $arSelectOrm);
		} else { 
			return '`'. filter_var($arSelect, FILTER_SANITIZE_STRING) .'`';
		} 

		return self::$instance;
	}
	
	public function Where($arWhere = false, $type = 'AND'){
		if(!$arWhere){
			if(count(self::$arData) <= 0)
				return false;

			$arWhere = self::$arData;
			self::$arData = array();
		}

		$arWhereOrm = array();
		if(is_array($arWhere)) foreach($arWhere as $key => $value){
			$arWhereOrm[] = ' `'.filter_var($key, FILTER_SANITIZE_STRING).'` = '.$this->Registry->DataBase->EscapeString($value);
		}
		
		if(!empty(self::$arQuery['WHERE']))
			self::$arQuery['WHERE'] = self::$arQuery['WHERE'] . ' ' . join(' '. $type .' ', $arWhereOrm);
		else
			self::$arQuery['WHERE'] = join(' '. $type .' ', $arWhereOrm);
		
		return self::$instance;
	}

	public function Count($Param = '*'){
		if(!isset(self::$arQuery['FROM']))
			return false;

		$arQuery = $this->Registry->DataBase->GetRow($this->Registry->DataBase->Query("SELECT COUNT(". $Param .") as count FROM `". self::$arQuery['FROM'] ."`". (isset(self::$arQuery['WHERE']) ? ' '. self::$arQuery['WHERE'] : ''))); 
		if(!$arQuery['count'])
			return false;
		
		return (int) $arQuery['count'];
	}

	public function Update(){
		if(count(self::$arData) <= 0)
				return false;

		$arSetOrm = array();
		foreach(self::$arData as $key => $value){
			$arSetOrm[] = '`'.$key.'` = '. $this->Registry->DataBase->EscapeString($value);
		}

		self::$arQuery['UPDATE'] = true;
		self::$arQuery['SET'] = join(', ', $arSetOrm);
		self::$arData = array();
		unset($arSetOrm);

		$this->Registry->DataBase->count++;

		$Query = $this->GetQueryPrepare();
		if(!$this->Registry->DataBase->Query($Query))
			return false;
		
		return true;
	}

	public function Insert(){
		if(count(self::$arData) <= false)
			return false;

		$arInsertOrm = array();
		foreach(self::$arData as $key => $value){
			$arInsertOrm['INSERT'][] = '`'. $key .'`';
			$arInsertOrm['VALUES'][] = $this->Registry->DataBase->EscapeString($value);
		}
		
		self::$arQuery['INSERT INTO'] = true;
		self::$arQuery['FIELD'] = '('. join(', ', $arInsertOrm['INSERT']).')';
		self::$arQuery['VALUES'] = '('. join(',', $arInsertOrm['VALUES']) .')';
		self::$arData = array();

		$Query = $this->GetQueryPrepare();
		if(!$this->Registry->DataBase->Query($Query))
			return false;
		
		return true;
	}

	public function Query(){
		$Query = $this->GetQueryPrepare();
		if(!$obQuery = $this->Registry->DataBase->Query($Query))
			return false;

		return $obQuery;
	}

	private function GetQueryPrepare(){
		$arQuery = array();
		$TableName = '';

		echo "<pre>";
		var_dump(self::$arQuery);
		echo "</pre>";
		$fromDelete = false;
		if(!empty(self::$arQuery) && is_array(self::$arQuery) && count(self::$arQuery) > 0)
			foreach(self::$arQuery as $Process => $Values){
				if(empty($Values) && $Values !== true) continue;
				switch($Process){
					case 'INSERT INTO':
						$Values = $TableName = self::$arQuery['FROM'];
						self::$arQuery['FROM'] = '';
						$Values = ''.$Values.'';
						$fromDelete = true;
						unset(self::$arQuery['FIELD'], self::$arQuery['VALUES']);
						break;
					case 'FIELD':
						$Process = $Values;
						$Values = '';
						break;
					case 'FROM':
						if($fromDelete == true) $Process = $Values = '';
						break;
					case 'UPDATE' : 
						$Values = $TableName = self::$arQuery['FROM'];
						self::$arQuery['FROM'] = '';
						$fromDelete = true;
						break;
				}

				$arQuery[] = trim($Process . ($Values !== true ? ' '. $Values : ''));
			}

		if(!empty($TableName)){
			self::$arQuery['FROM'] = &$TableName;
			if($fromDelete == true) $fromDelete = false;
			unset($TableName);
		}
		echo "<pre>";
		var_dump(join(' ', $arQuery));
		echo "</pre>";
		return trim(join(' ', $arQuery));
	}

	public function __set($key, $value){
		if(isset(self::$arData[$key]))
			die('error');

		self::$arData[$key] = $value;
	}

	public function __get($key){
		if(isset(self::$arData[$key]))
			return self::$arData[$key];
		else
			return false;
	}
}