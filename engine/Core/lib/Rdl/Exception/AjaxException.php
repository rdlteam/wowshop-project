<?php
namespace Rdl\Exception;

class AjaxException extends \Exception {
	private $CodeError = false;

	function __construct($Code, $Message){
		if(is_array($Message))
			$Message = json_encode($Message);
		else{
			$arData = array('Error' => 'true', 'ErrorCode' => $Code, 'message' => $Message);
			if($Code == '13001'){
				$arData['csfr_detect'] = \Rdl\Security\Security::getInstance()->getPostString();
			}
			if($Code == 200)
				unset($arData['Error'], $arData['ErrorCode']);
			
			$Message = json_encode($arData);
		}

		parent::__construct($Message);
	}
	public function Display(){
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
			return $this->getMessage();
		}
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH'])){
			return $this->getMessage();
		}
	}
}