<?php
namespace Rdl\Exception;

class ErrorPage extends \Exception {
	private $ErrorCode = array(

		);

	function __construct($errorCode){
		parent::__construct($errorCode);
	}

	function Display(){
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']))
			return json_encode(array('error' => 'true', 'message' => $this->getMessage(), 'line' => $this->getLine()));
		else{
			$template = new \Rdl\View\Template();
			$template->Display('404-page');
		}
	}
}