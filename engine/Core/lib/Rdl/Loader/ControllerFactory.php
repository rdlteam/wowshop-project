<?php
namespace Rdl\Loader;

use Rdl\View\Template,
	ReflectionClass;

abstract class ControllerFactory extends \Rdl\CoreFactory {
	
	protected $arParams = array();
	protected $controllerPath = false;
	protected $ControllerName = false;
	protected $TypeComponent  = false;
	protected $cTemplate	  = false;
	protected $ControllerTemplate = false; 

	protected $reflection = null;

	private $Model = false;

	final public function __construct($arParams = array(), $typeComponent = false){
		parent::__construct();

		$this->arParams = $arParams; 
		if($typeComponent !== false)
			$this->TypeComponent = $typeComponent;

		$this->Template = &$this->Registry->Template;

		$this->reflection = new ReflectionClass(get_called_class());
		$this->ControllerName = str_replace('Controller', '', $this->reflection->getShortName());
		$this->controllerPath = dirname($this->reflection->getFileName());

		$this->Options();
		$this->ReloadControllerPath();
	}

	abstract protected function Options();

	protected function ReloadControllerPath($template = 'default'){
		
		if(!empty($this->arParams['Template']))
			$template = $this->arParams['Template'];

		if(!$this->ControllerTemplate)
			$this->ControllerTemplate = new Template();

		$arPath = array(
				'template'	=> ENGINE_DIR . 'templates/' . $this->Core->Config->Site->Template .'/components/'. $this->TypeComponent .'/'.$this->ControllerName . '/' . $template .'/', 
				'component' => $this->controllerPath .'/templates/' . $template .'/'
			);
		foreach($arPath as $Key => $Path){
			if(is_dir($Path)) {
				$this->ControllerTemplate->_path = $Path;
				
				if($Key == 'component')
					$Path = 'engine/' . $Path;

				$this->ControllerTemplate->Template = $this->Core->Url . '/'. str_replace(ENGINE_DIR, '', $Path);
				break;
			}
		}
	}
	
	public function PathComponentTemplate(){
		return '';
	}

	public function LoadModel($model, array $arParams = array()){
		if(empty($model)) return false;
		$Model = explode(':', $model, 2);
		$ModelPath = (in_array($Model[0], array('global', 'Global'))) ? ENGINE_DIR .'/Models/'.ucfirst($Model[1]).'.model.php' : ENGINE_DIR .'/components/'. ucfirst($Model[0]) .'/'. $Model[1] .'/model.php';
		if(!file_exists($ModelPath))
			throw new \Rdl\Exception\ErrorPage('301 model');

		require_once($ModelPath);
		$ModelClass = ucfirst($Model[1]).'Model';
		
		if(!class_exists($ModelClass))
			throw new \Rdl\Exception\ErrorPage('301');

		$objModel = new $ModelClass;
		
		if(count($objModel->arParams) > 0)
			$objModel->arParams = array_merge($objModel->arParams, $arParams);
		else
			$objModel->arParams = $arParams;

		return $objModel;
	}

	public function thisModel($reload = false){
		if(!$this->controllerPath || !$this->ControllerName) 
			return false;
		
		$Path = $this->controllerPath .'/model.php';

		if(!file_exists($Path))
			throw new \Rdl\Exception\ErrorPage('Error load Model '. $Path);

		require_once($Path);
		$ModelClass = ucfirst($this->ControllerName).'Model';
		
		if(!class_exists($ModelClass))
			throw new \Rdl\Exception\ErrorPage('Error load Model '. $Path .'. no loading object'. $ModelClass);
		
		if($this->Model == false OR $reload == true)
			$this->Model = new $ModelClass($this->arParams);

		if(!empty($this->arParams))
			$this->Model->arParams = $this->arParams;

		return $this->Model;
	}	

	public function getParams($arParams = array()){
		$this->arParams = $arParams;
	}

	public function getTypeController($type = false){
		if(!$type) return false;
		$this->TypeComponent = $type;
	}
	public function GetTypeComponent($type = false){
		$this->getTypeController($type);
	}
	public function GetControllerName($name = false){
		if(!$name) return false;
		$this->ControllerName = $name;
	}
}