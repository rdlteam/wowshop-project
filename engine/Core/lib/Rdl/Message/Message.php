<?php
namespace Rdl\Message;

class Message extends Rdl\CoreFactory {
	
	static private $instance = false;
	protected $SessName = 'Message';

	public function getInstance(){
		if(self::$instance === false)
			self::$instance = new self;

		return self::$instance;
	}

	public function Set($SessName){
		$this->SessName = filter_var($SessName, FITLER_SANITIZE_STRING);

		return $this->getInstance();
	}

	
	public function Get(){
		return $this->SessName;
	}

	public function AddMessage($Message, $Title = false){
		if($Title !== false)
			$Message = array(
				'title' => filter_var($Title, FITLER_SANITIZE_STRING), 
				'message' => filter_var($Message, FITLER_SANITIZE_STRING)
			);

		$this->Registry->SessionStorage->{$this->SessName} = $Message;

		return $this->getInstance();
	}

	public function ShowMessage(){
		$obSession = $this->Registry->SessionStorage->{$this->SessName};
		unset($this->Registry->SessionStorage->{$this->SessName});
		if(is_array($obSession))
			return $obSession;
		
		return $obSession;
	}
}