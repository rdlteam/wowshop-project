<?php
namespace Rdl\Module;

class Standart {
	
	const MODULE_DIR = 'Modules/';

	function __construct(){
		$this->Core = \RDLCore::getInstance();
		$this->CacheCore = new \Rdl\Cache\CacheStorage();
	}

	public function run(){
		$this->LoadModule();
	}

	public function LoadModule($Module = false, $Path = false){
		if(!$Path) 
			$Path = ENGINE_DIR . self::MODULE_DIR;

		echo $Path;
	}
}