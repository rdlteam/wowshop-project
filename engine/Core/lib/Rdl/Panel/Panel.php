<?php
namespace Rdl\Panel;

use \Rdl\Components\Loader;

class Panel extends \Rdl\CoreFactory{
	public $vars = array();
	public $Params = array('Component' => 'Engine');

	function __construct(){}

	public function GetNavigation(){

	}
	
	public function addLink($link, $name, $icon){
		if(is_array($link)){
			foreach($link as $id => $Param){
				
				if(empty($Param['link'])) $Param['link'] = '#';
				if(empty($Param['name'])) $Param['name'] = 'link';
				if(empty($Param['icon'])) $Param['icon'] = '#';

 				$this->addLink($Param['link'], $Param['name'], $Param['icon']);
			}
		}else{
			$vars[] = array(
				'icon' => $icon,
				'name' => $name,
				'link' => $link
			);
		}
	}

	public function Show(){
		if(is_dir(ENGINE_DIR . DIRECTORY_SEPARATOR . 'components' . DIRECTORY_SEPARATOR . $this->Params['Component'] . 'panel')){
			return $this->CLoad($this->Params['Component'] .':panel');
		} else {
			return $this->CLoad('Engine:panel');
		}
	}
}