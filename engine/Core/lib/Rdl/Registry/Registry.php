<?php
class Registry {

	private static $instance = false;
	private $arRegistry = array();

	public static function getInstance(){
		if(!self::$instance)
			self::$instance = new self;

		return self::$instance;
	}
	
	private function __construct() {}
	private function __wakeup() {} 

	function __set($objectName, $value){
		if(empty($value)) return false;
		$this->arRegistry[$objectName] = $value;
	}

	function __get($objectName){
		return (isset($this->arRegistry[$objectName])) ? $this->arRegistry[$objectName] : false;
	}
}