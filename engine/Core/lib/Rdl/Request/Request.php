<?php
namespace Rdl\Request;

Class Request extends \Rdl\CoreFactory{
        
    public static function Get($var = '', $save = false, $default_value = false){
        return self::GetParam(array('var' => $var, 'save' => $save, 'method' => 'GET', 'default' => $default_value));
    }
    
    public static function Post($var = '', $save = false, $default_value = false){
        return self::GetParam(array('var' => $var, 'save' => $save, 'method' => 'POST', 'default' => $default_value));
    }
    
    public static function GetParam(array $param = array()){
        if(!is_array($param))
            return false;

        if(empty($param['method']) OR !$param['method'])
            $param['method'] = 'GET';

        else if(!in_array($param['method'], array('GET', 'POST', 'REQUEST', 'SERVER', 'COOKIE')))
            return false;

        if(empty($param['var']) && !empty($param['method']))
            $param['var'] = self::method(false, $param['method']);
        
        if(empty($param['save']))
            $param['save'] = false;


        if(is_array($param['var']) && count($param['var']) > 0){

            $return = array();

            foreach($var as $key => $value){
                //Проверяем, глобальные настройки, или же персональные для каждой переменной
                if(is_array($param['save']) && isset($param['save'][$value])) 
                    $save = $param['save'][$value];

                if(!($return[$value] = self::GetParam(
                                            array(
                                                'var' => $value, 
                                                'save' => $param['save']
                                            )
                                        ))){ //self::GetParam($value, $save, $flags))){
                    return false;
                }
            }

            return $return;

        } else {
            
            if(!self::method($param['method'], $param['var'])) 
                return !isset($param['default']) ? false : $param['default'];

            return $param['save'] == true ? self::Filter(self::method($param['method'], $param['var']), $param['save']) : self::method($param['method'], $param['var']);

        }
    }

    public static function method($method = 'GET', $var = false){
        switch(mb_strtoupper($method)){
            case 'GET':
                if(empty($var) OR !$var)
                    return $_GET;

                return isset($_GET[$var]) ? $_GET[$var] : false;
                break;
            case 'POST':
                 if(empty($var) OR !$var)
                    return $_POST;

                return isset($_POST[$var]) ? $_POST[$var] : false;
                break;
        }
    }

    static function Filter($value, $type = 'default'){
        if(empty($value))
            return false;

        $return = array();

        switch(mb_strtolower($type)){
            case "int":
                $return['filter'] = FILTER_SANITIZE_NUMBER_INT;
            break;
            case "intval":
                $return['filter'] = FILTER_VALIDATE_INT;
            break;
            case "float":
                $return['filter'] = FILTER_SANITIZE_NUMBER_FLOAT;
            break;
            case "floatval":
                $return['filter'] = FILTER_VALIDATE_FLOAT;
            break;
            default:
               $return['filter'] = array('filter' => FILTER_SANITIZE_STRING);
            break;

        }

        if(is_array($return['filter']))
            return filter_var($value, $return['filter']['filter']);
        
        return filter_var($value, $return['filter']);
    }

    public static function isPost(){
        return $_POST ? true : false;
    }

    public static function Header($value){
        if(!headers_sent())
            header($value);
        else
            return false;
    }
}
?>