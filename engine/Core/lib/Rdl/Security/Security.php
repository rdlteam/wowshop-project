<?php
namespace Rdl\Security;

use Rdl\Request\Request;

class Security extends \Rdl\CoreFactory{
	private static $instance = false;
    const TOKEN_CHARACTERS = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const TOKEN_LENGTH = 32;
    const N_SESSION = 'csrf_';
    const N_KEY = '_csrf_key';
    const N_TOKEN = '_csrf_token';

    private $key;
    private $token;

    public static function getInstance(){
		if(!self::$instance) 
			self::$instance = new self;

		return self::$instance;
	}

    /**
     * Create a new key/token pair and save it in the current session.
     */
    public function __construct() {
        parent::__construct();
        do {
            $key = self::generateToken();
        } while ($this->Registry->SessionStorage->{self::N_SESSION . $key});

        $this->key = $key;
        $this->token = self::generateToken();

        $this->Registry->SessionStorage->{self::N_SESSION . $key} = $this->token;
    }

    /**
     * Get the key of this pair.
     */
    public function getKey() {
        return $this->key;
    }

    /**
     * Get the token of this pair.
     */
    public function getToken() {
        return $this->token;
    }

    /**
     * Get a string that can be inserted into an html <form>.
     */
    public function getPostString() {
        return '<input class="csrf_detected_input" type="hidden" name="' . self::N_KEY . '" value="' . $this->key . '" />'
            . '<input class="csrf_detected_input" type="hidden" name="' . self::N_TOKEN . '" value="' . $this->token . '" />';
    }

    /**
     * Get a string that can be appended to an URL.
     */
    public function getGetString() {
        return self::N_KEY . '=' . $this->key . '&' . self::N_TOKEN . '=' . $this->token;
    }

    /**
     * Verify a key/token pair.
     *
     * Both are loaded automatically from POST or GET parameters (in this order).
     * A match in the session will be deleted so this method can return only for
     * the fist invocation.
     */
    public function verifyToken() {
        if(Request::Post(self::N_KEY) && Request::Post(self::N_TOKEN)){
        	$key = Request::Post(self::N_KEY);
        	$token = Request::Post(self::N_TOKEN);
        } else if(Request::Get(self::N_KEY) && Request::Get(self::N_TOKEN)){
        	$key = Request::Get(self::N_KEY);
        	$token = Request::Get(self::N_TOKEN);
        } else 
        	return false;

        $sessionToken = $this->Registry->SessionStorage->{self::N_SESSION . $key};
        unset($this->Registry->SessionStorage->{self::N_SESSION . $key});

        return !empty($sessionToken) && $sessionToken === $token;
    }

    /**
     * Exit with http status code 400 if no valid pair was found.
     */
    public function verifyOrFail() {
        if (!$this->verifyToken()) {
            
            if($this->Core->Service == 'ajax')
                throw new \Rdl\Exception\AjaxException('13212', 'Ошибка токена');

            header('HTTP/1.0 403 CSRF ATTACK DETECTED!');
            echo 'CSRF ATTACK DETECTED!';
            die();
        }
    }

    /**
     * Generate an alphanumeric token.
     */
    private static function generateToken() {
        $chars = self::TOKEN_CHARACTERS;
        $maxRand = strlen($chars) - 1;

        $token = '';
        for ($i = 0; $i < self::TOKEN_LENGTH; $i++) {
            $token .= $chars{mt_rand(0, $maxRand)};
        }

        return $token;
    }
}