<?php
namespace Rdl\Session;

class Session {	
	public function Run(){
		session_start();
	}
	private function VerifySession(){
		if(!$this->hash) 
			return false;
		
		if($this->hash != sha1($this->Core->Url . $this->Core->Config->HashKey))
			return false;
		else 
			$this->hash;
	}
	function __set($Name, $Value){
		$_SESSION[$Name] = $Value;
	}

	function __get($Name){
		return isset($_SESSION[$Name]) ? $_SESSION[$Name] : false;
	}

	public function getSession(){
		return count($_SESSION) > 0 ? $_SESSION : false;
	}

	public function Destroy(){
		$_SESSION = array();
		session_destroy();
		session_unset();
	}
}