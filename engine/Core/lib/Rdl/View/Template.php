<?php
namespace Rdl\View;

use \Rdl\Registry\Registry;

class Template extends \Rdl\CoreFactory{
	
	public $_path;
	private $_template;
	private $_var = array();
	private $_Svar = array();

	public $Content = '';
	public $title = false;
	public $pageTitle = '';
	// public $thisPage = false;

	function __construct(){
		parent::__construct();

		$this->_Svar['thisPage'] = (!in_array($this->Registry->Route->RealPage, array('/', '/index.php'))) ? $this->Registry->Route->RealPage : false;

		$this->_path = ROOT_DIR . '/engine/templates/'. $this->Core->Config->Site->Template .'/';
		
		if($this->Core->Service == 'admin')
			$this->Core->Config->Site->Template = 'adminv3';

		$this->_Svar['Template'] = $this->_Svar['GlobalTemplate'] = $this->Core->Url . '/engine/templates/'. $this->Core->Config->Site->Template;
		$this->_Svar['baseUrl'] = $this->Core->Url;
		$this->_Svar['Helper'] = $this->Registry->Helper;
	}

	public function set($name, $value)
	{
		$this->_var[$name] = $value;
	}
	
	public function getThisTitle(){
		// var_dump($this->title);
		return end($this->title);
	}

	public function __get($name)
	{
		if (!empty($this->_var[$name])) 
			return $this->_var[$name];
		elseif(!empty($this->_Svar[$name]))
			return $this->_Svar[$name];

		return '';
	}

	public function display($template = 'index', $strip = true)
	{
		$this->_template = $this->_path . $template .'.phtml';
		if (!file_exists($this->_template)) die('Шаблона ' . $this->_template . ' не существует!');

		ob_start();
		include_once($this->_template);
		echo ($strip) ? $this->_strip(ob_get_clean()) : ob_get_clean();
	}

	public function clear(){
		$this->_var = array();
	}

	private function _strip($data)
	{
		$lit = array("\\t", "\\n", "\\n\\r", "\\r\\n", "  ");
		$sp = array('', '', '', '', '');
		return str_replace($lit, $sp, $data);
	}
	public function xss($data)
	{
		if (is_array($data)) {
			$escaped = array();
			foreach ($data as $key => $value) {
				$escaped[$key] = $this->xss($value);
			}
			return $escaped;
		}
		return htmlspecialchars($data, ENT_QUOTES);
	}

	public function thisLink($link){
		if(strpos($link, '.php') !== false){
			if(sha1(trim($link)) == sha1(trim($this->Registry->Route->RealPage)))
				return true;
			else
				return false;
		} else {
			if(in_array(trim($this->Registry->Route->RealPage), array($link, $link.'index.php', $link .'/index.php')))
				return true;
			else
				return false;
		}
	}

	public function LoadComponent($component, $arParams = array(), $action = 'default'){
			$controller = explode(':', $component, 2);
			$path = ENGINE_DIR .'/components/'. ucfirst($controller[0]) .'/'. $controller[1] .'/controller.php';
			
			if(!file_exists($path))
				throw new \Rdl\Exception\ErrorPage('301 5');

			require_once $path;
			$className = ucfirst($controller[1]) .'Controller';

			if(!class_exists($className))
				throw new \Rdl\Exception\ErrorPage('301 22');

			$Class = new $className($arParams, ucfirst($controller[0]));

			$method = $action.'Include';
			if(!method_exists($Class, $method))
				throw new \Rdl\Exception\ErrorPage('301 2');

			ob_start();
			$Class->{$method}();
			$return = ob_get_contents();
			ob_end_clean();
			unset($Class);
			return $return;
	}
}