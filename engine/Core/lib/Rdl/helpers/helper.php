<?php
namespace Rdl\helpers;

class helper extends \Rdl\CoreFactory {
	private static $arJSFiles = false;
	private static $arJSFilesCount = 0;
	private static $splStartArrayCount = 1;
	function __construct(){
		parent::__construct();
		if(!self::$arJSFiles)
			self::$arJSFiles = new \SplFixedArray(self::$splStartArrayCount);

	}

	public static function newJS($path){
		self::$arJSFiles[self::getCount()] = $path;
		self::$arJSFilesCount++;
		if(self::$splStartArrayCount <= self::getCount())
			self::$arJSFiles->setSize(self::getCount() + 1);
	}
	
	public function ShowScript(){
		if(count(self::$arJSFiles) > 1){
			foreach(self::$arJSFiles as $key => $value)
				if(!empty($value))
					$this->addScript($value);
		}
	}

	public function addScript($url = false){
		if(empty($url)) return false;
		echo "\r\n<script type=\"text/javascript\" src=\"".$url."\" ></script>\r\n";
		return true;
	}

	public static function getCount(){
		return static::$arJSFilesCount;
	}

	public function Get(){
		return self::$arJSFiles;
	}
}