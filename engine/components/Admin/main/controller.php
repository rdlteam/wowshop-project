<?php

use \Rdl\View\Template;

class MainController extends Rdl\Loader\ControllerFactory {
	
	public function Options(){
		$this->controllerPath = dirname(__FILE__);
		$this->ControllerName = 'main';
		
		$this->ReloadControllerPath();
	}

	public function IndexAction(){
		echo $this->ControllerTemplate->Display('index');
		// die('test');
	}

}
