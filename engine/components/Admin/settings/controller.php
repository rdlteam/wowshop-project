<?php
/**
* RDL CMS Settings Controller
* Контроллер управления настройками сайта
*
* @link      http://www.rdl-team.ru/ official site
* @copyright Copyright (c) 2013 RDL Team. (http://www.rdl-team.ru)
* @license   http://www.rdl-team.ru/license/new-bsd New BSD License
*/

use Rdl\Request\Request;
class SettingsController extends Rdl\Loader\ControllerFactory {
	
	public function Options(){
		$this->Registry->SpeedBar['/settings/'] = array('title' => 'Настройки', 'desc' => 'Общие настройки');
	}

	public function IndexAction(){
		$this->thisModel()->SaveSettings();
		$this->Registry->SpeedBar['/settings/site/'] = array('title' => 'Настройки сайта');
		$this->ControllerTemplate->Display('index');
	}

	public function RedirectAction(){
		Request::Header('Location: /admin/settings/site/');
		die();
	}
}
