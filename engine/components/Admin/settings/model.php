<?php

use Rdl\Request\Request;
use Rdl\Components\Loader;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\TableGateway\Feature\RowGatewayFeature;
use Rdl\Exception\ErrorPage;
use Zend\Config\Config;
use Zend\Config\Writer\PhpArray;
use Rdl\Exception\AjaxException;
use Rdl\Security\Security;

class SettingsModel extends \Rdl\Loader\ModelFactory {
	public function SaveSettings(){
		if(!Request::Post('submit') && !($this->Core->isAjax() && Request::Post('title'))){
			return false;
		}

		Security::getInstance()->verifyOrFail();
		
		$config = new Config(array(), true);
		
		$config->protocol = $this->Core->Config->protocol;

		$config->db = array();
		$config->Site = array();
		$config->Site->meta = array();
		$config->Session = array();

		if(Request::Post('title'))
			$config->Site->title = Request::Post('title');

		if(Request::Post('url'))
			$config->Site->url   = Request::Post('url');
		
		if(Request::Post('template'))
			$config->Site->Template = Request::Post('template');

		if(Request::Post('description'))
			$config->Site->meta->description = Request::Post('description');

		if(Request::Post('keywords'))
			$config->Site->meta->keywords 	 = join(',', array_unique(explode(',', Request::Post('keywords'))));

		if(Request::Post('cache'))
			$config->cache = (Request::Post('cache') == 1);
		else
			$config->cache = false;

		if(Request::Post('minify'))
			$config->minify = (Request::Post('minify') == 1);
		else
			$config->minify = false;

		$config->charset = Request::Post('charset') ? Request::Post('charset') : 'utf-8'; 

		// Оставляем не изменными данные соединения с базой
		$config->db->driver = $this->Core->Config->db->driver;
		$config->db->database = $this->Core->Config->db->database;
		$config->db->username = $this->Core->Config->db->username;
		$config->db->password = $this->Core->Config->db->password;

		// Оставляем не изменным защиту ключа
		if(!$config->HashKey = Request::Post('HashKey'))
			$config->HashKey = $this->Core->Config->HashKey;

		$config->Session->Validate = Request::Post('session_validate');
		$config->Session->Storage = Request::Post('session_storage');
		$config->remember = (Request::Post('remember') == 1);

		if(!$config->remember)
			$this->Registry->Auth->ClearRemember();

		$writer = new PhpArray();
		if(!$this->SaveConfig($writer->toString($config))){
			$arEncode = array('class' => 'error', 'message' => 'Ошибка при сохранении конфигурационного файла', 'csfr_detect' => \Rdl\Security\Security::getInstance()->getPostString());
			if($this->Core->isAjax())
				die(json_encode($arEncode));

			$this->Registry->SessionStorage->Message = json_encode($arEncode);
		} else {
			$arEncode = array('class' => 'success', 'message' => 'Новые настройки системы успешно сохранены', 'csfr_detect' => \Rdl\Security\Security::getInstance()->getPostString());
			if($this->Core->isAjax())
				die(json_encode($arEncode));

			$this->Registry->SessionStorage->Message = json_encode($arEncode);
		}

		header('Location: '. $_SERVER['REQUEST_URI']);
		die();
	}

	private function SaveConfig($value){
		$cache_file = ENGINE_DIR .'Core/config/global.config.php';
		
		if(!file_exists($cache_file))
			return false;

        $f = fopen($cache_file, "w+");
        @chmod($cache_file, 0666);
        fwrite($f, $value);
        fclose($f);

        return true;
	}
}
