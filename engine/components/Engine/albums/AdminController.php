<?php
use \Rdl\Cache\CacheStorage;
Class AlbumsController extends Rdl\Loader\ControllerFactory  {
	
	public function Options(){
		$this->controllerPath = dirname(__FILE__);
		$this->ControllerName = 'albums';
		$this->SpeedBar[] = 'Альбомы';

		$this->ReloadControllerPath('admin');
	}

	public function IndexAction(){
		$this->ControllerTemplate->Set('arAlbums', $this->thisModel()->GetAlbums());
		$this->ControllerTemplate->Display('index');
	}
}