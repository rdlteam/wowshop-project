<?php

use Rdl\Request\Request;

class AuthController extends Rdl\Loader\ControllerFactory {
	
	public function Options(){
		$this->controllerPath = dirname(__FILE__);
		$this->ControllerName = 'auth';

		$this->ReloadControllerPath();
	}

	public function loginAction(){
		if(!in_array($this->Core->Service, array('admin', 'index')))
			return false;

		$this->ControllerTemplate->Display('index');
	}

	public function defaultInclude(){
		if($this->Registry->Auth->isLogin() && !$this->Registry->Auth->isLock()){
			$this->ControllerTemplate->Display('cabinet/include');
			return;
		}
		$this->loginAction();	
	}
}