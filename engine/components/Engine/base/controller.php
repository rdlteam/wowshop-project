<?php

use \Rdl\View\Template;

class BaseController extends Rdl\Loader\ControllerFactory {
	
	protected $ControllerTemplate = false;
	protected $arArea = array(
			1 => array(
				'class' => 'br',
				'name'	=> 'Братеево'
			),
			2 => array(
				'class' => 'dl',
				'name'	=> 'Даниловский'
			),
			3 => array(
				'class' => 'ds',
				'name'	=> 'Донской'
			),
			4 => array(
				'class' => 'ms',
				'name'	=> 'Москворечье-Сабурово'
			), 
			5 => array(
				'class' => 'ns',
				'name'	=> 'Нагатино-Садовники'
			),
			6 => array(
				'class' => 'nz',
				'name'	=> 'Нагатинский затон'
			)
		);

	public function Options(){
		$this->controllerPath = dirname(__FILE__);
		$this->ControllerName = 'base';
		if(!$this->ControllerTemplate)
			$this->ControllerTemplate = new Template();

		$path = ENGINE_DIR .'templates'. DIRECTORY_SEPARATOR .$this->Core->Config->Site->Template. DIRECTORY_SEPARATOR .'components'. DIRECTORY_SEPARATOR .$this->TypeComponent. DIRECTORY_SEPARATOR . $this->ControllerName . DIRECTORY_SEPARATOR;
		$this->ControllerTemplate->_path = is_dir($path) ? $path : $this->controllerPath . DIRECTORY_SEPARATOR .'templates'. DIRECTORY_SEPARATOR . (!$this->cTemplate ? 'default' : '') . DIRECTORY_SEPARATOR;
		$this->ControllerTemplate->Template = $this->Core->Url .'/engine/components/'. $this->TypeComponent .'/'. $this->ControllerName .'/templates/'. (!$this->cTemplate ? 'default' : '') .'/';
		
	}

	public function IndexAjaxAction(){
		$Model = $this->thisModel();
		if(!($arData = $Model->Query()))
			die('К сожалению ваш запрос не дал результата');
				
		$this->ControllerTemplate->Set('Area', $this->arArea);
		$this->ControllerTemplate->Set('Count', $Model->Count());
		$this->ControllerTemplate->Set('arData', $arData);
		$this->ControllerTemplate->Display();
		die();
	}

	public function SaveParamAjaxAction(){
		return $this->thisModel()->SaveParams();
	}
	public function dataCacheAjaxAction(){
		return $this->thisModel()->dataCache();
	}

	public function DetailAjaxAction(){
		if(!($arDetail = $this->thisModel()->GetDetail()))
			die('Ошибка доступа к пользователю');

		$arDetail['area'] = $this->arArea[(int) $arDetail['area']]['name'];

		
		$byrsdayDate = new DateTime($arDetail['dateByrsday']);
		$thisDate = new DateTime('NOW');
		
		$arDetail['dateByrsday'] = array(
				'date' => $byrsdayDate->format('d.m.Y'),
				'years' => $byrsdayDate->diff($thisDate)->format('%Y%')
			);
		
		if($arDetail['passportDate'] != '0000-00-00'){
			$passportDate = new DateTime($arDetail['passportDate']);
			if($arDetail['dateByrsday']['years'] >= '20' && $passportDate->diff($thisDate)->format('%Y%') > 0)
				$arDetail['passportDate'] = '<span style="color: red" data-input="passportDate" contenteditable="true">'.$passportDate->format('d.m.Y').'</span>';
			else
				$arDetail['passportDate'] = '<span data-input="passportDate" contenteditable="true">'.$passportDate->format('d.m.Y').'</span>';
		} else 
			$arDetail['passportDate'] = '-';


		if($arDetail['appearance'] != '0000-00-00'){
			$obAppearance = new DateTime($arDetail['appearance']);
			$arDetail['appearance'] = $obAppearance->format('d.m.Y');
		} else 
			$arDetail['appearance'] = '-';


		$arDetail['street'] = $this->thisModel()->GetStreet($arDetail['street']);
		$arDetail['category'] = $this->thisModel()->GetType($arDetail['category']);
		
		if($arDetail['passportSerial'] == '0')
			$arDetail['passportSerial'] = '-';
		if($arDetail['passportNumber'] == '0')
			$arDetail['passportNumber']	= '-';

		$this->ControllerTemplate->Set('arData', $arDetail);
		$this->ControllerTemplate->Display('detail');
		die();
	}

	public function defaultInclude(){
		$this->Registry->Template->Display('form');
	}
}
