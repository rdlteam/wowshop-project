<?php
use Zend\Cache\StorageFactory;
use Rdl\Request\Request;
use Rdl\Components\Loader;
use Rdl\Components\File;
use Rdl\Components\Catalog\Catalog;
use Rdl\Components\Catalog\xFields;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\TableGateway\Feature\RowGatewayFeature;
use Rdl\Exception\ErrorPage;
use Zend\Mail\Message;
use Zend\Mail\Transport\Sendmail as SendmailTransport;

class BasketModel extends \Rdl\Loader\ModelFactory {
	public function GetItems(){
		if(!isset($this->Registry->SessionStorage->basket['items']) || empty($this->Registry->SessionStorage->basket['items']))
			return array();

		$obData = new TableGateway('rdl_catalog', $this->Registry->DataBase);
		$arReturn = array();
		foreach($obData->Select(array('id' => $this->GetItemsIDs()))->toArray() as $key => $arItem){
			$arImages[$arItem['id']] = $arItem['detail_image'];
			$arReturn[$arItem['id']] = $arItem;
		}

		$obFile = new File;
		$arFiles = $obFile->getImages(array_values($arImages), '200x0');
		foreach($arImages as $key => $value){
			if(count($arImages) <= 1)
				$arReturn[$key]['detail_image'] = $arFiles;
			else	
				$arReturn[$key]['detail_image'] = $arFiles[$value];
		}
		unset($arFiles, $arImages);
		return $arReturn;
	}

	public function GetItemsIDs(){
		if(empty($this->Registry->SessionStorage->basket['items']))
			return false;

		$arReturn = array();
		foreach($this->Registry->SessionStorage->basket['items'] as $key => $arItem){
			$arReturn = explode('_', $key, 2)[1];
		}

		return $arReturn;
	}

	public function SendBasket(){

		$FullName = array(Request::Post('lastname'), Request::Post('fullname'), Request::Post('otvname'));
		$obData = new TableGateway('rdl_orders', $this->Registry->DataBase);
		$date = new \DateTime();
		$obData->insert(array(
			'user_id' => $this->Registry->Auth->id, 
			'fullname' => join(' ', $FullName),
			'city' => Request::Post('city'),
			'street' => Request::Post('adress'),
			'phone' => Request::Post('mobile'),
			'email'  => Request::Post('email'),
			'dateByrsday' => Request::Post('byrsday'),
			'comment' => Request::Post('comment'),
			'deliveryType' => Request::Post('pay_type'),
			'dateTime' => $date->format('Y-m-d H:i:s'),
			'deliveryDate' => $date->modify('+1 day')->format('Y-m-d'),
			'price' =>  $this->Registry->SessionStorage->basket['Money']
			)
		);

		$obDataItems = new TableGateway('rdl_order_items', $this->Registry->DataBase);
		foreach($this->Registry->SessionStorage->basket['items'] as $key => $arItem){
			$item = explode('_', $key, 2);
			$obDataItems->insert(array(
				'order_id' => $obData->lastInsertValue,
				'item_id' => $arItem['item_id'],
				'xfields' => json_encode(array('size' => $item[0])),
				'count' => $arItem['count'],
				'cost' => $arItem['price']
				));
		}
		/*
		$obMessage = new Message();
		$obMessage->addFrom("localhost@localhost.ru")
				->addTo(Request::Post('email'))
        		->setSubject("Ваш заказ на Wow Shop")
        		->setBody("Вы сделали заказ на сайте WOW Shop\n Номер вашего заказа: ". $obData->lastInsertValue);

		$obTransport = new SendmailTransport();
		$obTransport->send($obMessage);

		$obMessage = new Message();
		$obMessage->addFrom("localhost@localhost.ru")
				->addTo('rdlrobot@gmail.com')
        		->setSubject("Сделан заказ")
        		->setBody("На сайте WOW Shop\n был сделан заказ, номер заказа: ". $obData->lastInsertValue);

		$obTransport = new SendmailTransport();
		$obTransport->send($obMessage);*/

		return $obData->lastInsertValue;
	}
}