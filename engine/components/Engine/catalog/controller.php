<?php
use Rdl\Request\Request;

class CatalogController extends Rdl\Loader\ControllerFactory{

	
	public function Options(){
		$this->Registry->SpeedBar[$this->Registry->Route->RealPage] = 'Каталог';
	}

	public function IndexAction(){
		if(!empty($this->Registry->Route->Params['ID'])){
			echo $this->CLoad('Engine:catalogdetail', '', array('ID' => (int) $this->Registry->Route->Params['ID']));
			return;
		}

		$arData = $this->thisModel()->GetData();
		$this->ControllerTemplate->Set('CountItem', $arData['Count']);
		$this->ControllerTemplate->Set('CategoryName', empty($arData['CategoryName']) ? 'Каталог' : $arData['CategoryName']);
		$this->ControllerTemplate->Set('arItems', $arData['arItems']);
		$this->ControllerTemplate->Display('index');
	}
	public function DefaultInclude(){
		 $this->ControllerTemplate->Set('arCatalog', $this->thisModel()->GetInclude(4));
		 $this->ControllerTemplate->Set('Count', $this->thisModel()->Count);
		 $this->ControllerTemplate->Display('include2');
		 $this->ControllerTemplate->Clear();
	}

	public function PopularInclude(){
		 $this->ControllerTemplate->Set('arCatalog', $this->thisModel()->GetIncludePopular(4));
		 $this->ControllerTemplate->Display('include');
		 $this->ControllerTemplate->Clear();
	}
	public function FilterInclude(){
		 $this->ControllerTemplate->Set('Selected', (int) Request::Get('sort'));
		 $this->ControllerTemplate->Display('filter');
		 $this->ControllerTemplate->Clear();
	}

	public function DoOrdersAjaxAction(){
		$this->thisModel()->UpdateOrder();
	}
}
