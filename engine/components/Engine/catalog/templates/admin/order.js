var title = "Изменение статуса заказа";
$(document).ready(function(){
	connect = 0;
	$('.change_status').on('click', function(){
		if(connect != 0)
			return false;

		var status = $(this).data('status');
		var order  = $(this).data('order');
		connect = 1;
		$('.panel-tools button').attr('disabled', true);
		$(this).attr('disabled', false);
		$('.loading_progress').fadeIn();
		$.ajax({
    		type: 'POST',
    		url: '/admin/catalog/orders/',
    		data: 'action=update_order&order_id=' + order + '&status='+ status,
    		dataType: 'json',
    		success: function(data){
    			$('.status_order' + order).html(data.message);
    			connect = 0;
    			$('.panel-tools button').attr('disabled', false);
				$('.loading_progress').fadeOut();
				if(status == 2)
					toastr.success("Заказ № "+ order +" был доставлен", title);
				else if(status == 1)
					toastr.info("Заказ № "+ order +" был подтвержден", title);
				else if(status == 0)
					toastr.warning("Заказ № "+ order +" не был обработан", title);
				else if(status == 5)
					toastr.error("Заказ № "+ order +" был отменен", title);

    		}
  		});
		
    	return false;
	});
});