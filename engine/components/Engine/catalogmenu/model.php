<?php
use Zend\Cache\StorageFactory;
use Rdl\Request\Request;
use Rdl\Components\Loader;
use Rdl\Components\File;
use Rdl\Components\Catalog\Catalog;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\TableGateway\Feature\RowGatewayFeature;
use Rdl\Exception\ErrorPage;

class CatalogmenuModel extends \Rdl\Loader\ModelFactory {
	public function GetMenuList(){
		$obCatalog = new Catalog();
		$arReturn = array();
		$arCategoryList = $obCatalog->GetCategoryList();
		if(isset($this->Registry->Route->Params['parent_category'])){ 
			$arReturn['category_name'] = $obCatalog->getCategoryName($this->Registry->Route->Params['parent_category'])['name'];
			$arCategoryList = $obCatalog->getCategorys($this->Registry->Route->Params['parent_category']);
		}
		if(!$arCategoryList)
			return array();

		foreach($arCategoryList as $categoryID => $arCategory){
			if($arCategory['parent_category'] != 0){
				$arCategory['link'] = $this->Registry->Route->RealPage . $obCatalog->GetCategoryList($arCategory['parent_category'])['altname'] .'/'. $arCategory['altname'] . '/';
			}else{
				$arCategory['link'] = $this->Registry->Route->RealPage . $arCategory['altname'] . '/';
			}
			
			$arReturn['category'][$categoryID] = $arCategory; 
		}
		sort($arReturn['category']);
		unset($arCategoryList);
		return $arReturn;
	}
}