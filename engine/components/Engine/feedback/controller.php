<?php

use Zend\Mail;
use Rdl\Request\Request;

class FeedbackController extends Rdl\Loader\ControllerFactory{
	public function IndexAction(){
		if(!empty($_POST)){
			if(!($Message = $this->SendMessage())){

			}else{
				die('<div class="notification success closeable"><p><span>Успех!</span> Ваше сообщение отправлено</p></div>');
			}
		}

		$this->Registry->Template->Display('feedback');
	}

	private function SendMessage(){
		if(empty($_POST['comments'])){
			die('<div class="notification error closeable"><p><span>Ошбика!</span> Введите текст сообщения</p></div>');
		}
		if(empty($_POST['email'])){
			die('<div class="notification error closeable"><p><span>Ошбика!</span> Введите ваш Email</p></div>');
		}elseif(!Request::Post('email', 'email')){
			die('<div class="notification error closeable"><p><span>Ошбика!</span> Вы не верно ввели Email</p></div>');
		}
		$mail = new Mail\Message();
		$mail->setBody(Request::Post('comments', true));
		$mail->setFrom(Request::Post('email', true), (!Request::Post('name', true) ? 'Anonymous' : Request::Post('name', true)));
		$mail->addTo('support@rdl-team.ru', 'RDLTeam Support');
		$mail->setSubject('Сообщение через форму обратной связи RDL');

		$transport = new Mail\Transport\Sendmail();
		$transport->send($mail);
		return true;
	}
} 