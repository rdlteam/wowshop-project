<?php
use Rdl\Request\Request;

class MenuController extends Rdl\Loader\ControllerFactory{
	
	public function Options(){
		$this->controllerPath = dirname(__FILE__);
		$this->ControllerName = 'menu';
		$this->ReloadControllerPath();
	}
	public function DefaultInclude(){
		// var_dump($this->thisModel()->GetMenu());
		 $this->ControllerTemplate->Set('arMenu', $this->thisModel()->GetMenu());
		 $this->ControllerTemplate->Display('index');
		 $this->ControllerTemplate->Clear();
	}
}
