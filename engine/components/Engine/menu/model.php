<?php
use Zend\Cache\StorageFactory;
use Rdl\Request\Request;
use Rdl\Components\Loader;
use Rdl\Components\File;
use Rdl\Components\Menu;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\TableGateway\Feature\RowGatewayFeature;
use Rdl\Exception\ErrorPage;

class MenuModel extends \Rdl\Loader\ModelFactory{

	public $obMenu = false;

	public function __construct($arParams = array()){
		parent::__construct();
		$this->arParams = $arParams;

		$this->obMenu = new Menu;
		$this->CacheID = 'menu_' . $this->arParams['menu'];  
		$this->obMenu->CacheRun();
		if(Request::Get('clear_cache') == 'Y'){
			$this->obMenu->CacheClear();
		}
	}

	public function GetMenu(){
		return $this->obMenu->GetMenu($this->arParams['menu']);
	}
	
}