<?php
use \Rdl\Cache\CacheStorage;
Class NewsController extends Rdl\Loader\ControllerFactory  {
	
	public function Options(){
		$this->controllerPath = dirname(__FILE__);
		$this->ControllerName = 'news';
		$this->SpeedBar[] = 'Новости';
	}

	public function IndexAction(){
		if(isset($this->Registry->Route->Params['news_id']) || isset($this->Registry->Route->Params['news_url'])){
			$this->DetailAction();
			return;
		}
		$where = array();
		if(isset($this->Registry->Route->Params['category_id']))
			$where[] = '`category` regexp \'[[:<:]]('.(int) $this->Registry->Route->Params['category_id'].')[[:>:]]\'';

		$this->Registry->Template->Set('arNews', $this->thisModel()->Get($where, array('PageLimit' => 10, 'Nav' => true)));
		$this->Registry->Template->Display('news-list');
		$this->Registry->Template->Clear();
	}

	public function DetailAction(){
		if((isset($this->Registry->Route->Params['news_id']) && intval($this->Registry->Route->Params['news_id']) <= 0) && empty($this->Registry->Route->Params['news_url'])){
			echo 'Возможно вы ошиблись при переходе';
			return;
		}

		$ModelResult = $this->LoadModel('Engine:news')->GetFull($this->Registry->Route->Params);
		
		$this->Registry->Template->title = array($ModelResult['title']);
		$this->Registry->SpeedBar[]	= $ModelResult['title'];
		$this->Registry->Template->Set('arNews', $ModelResult);
		$this->Registry->Template->Display('news-detail');
		$this->Registry->Template->Clear();
	}

	public function defaultInclude(){
		$this->Registry->Template->Set('arNews', $this->thisModel()->Get(array(), array('PageLimit' => 2)));
		$this->Registry->Template->Display('news-list-index');
		$this->Registry->Template->Clear();
	}

}