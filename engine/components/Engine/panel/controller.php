<?php

use \Rdl\View\Template;

class PanelController extends Rdl\Loader\ControllerFactory {
	
	protected $ControllerTemplate = false;

	public function Options(){
		$this->controllerPath = dirname(__FILE__);
		$this->ControllerName = 'panel';
		if(!$this->ControllerTemplate)
			$this->ControllerTemplate = new Template();

		$path = ENGINE_DIR .'templates'. DIRECTORY_SEPARATOR .$this->Core->Config->Site->Template. DIRECTORY_SEPARATOR .'components'. DIRECTORY_SEPARATOR .$this->TypeComponent. DIRECTORY_SEPARATOR .'panel'. DIRECTORY_SEPARATOR;
		$this->ControllerTemplate->_path = is_dir($path) ? $path : $this->controllerPath . DIRECTORY_SEPARATOR .'templates'. DIRECTORY_SEPARATOR . (!$this->cTemplate ? 'default' : '') . DIRECTORY_SEPARATOR;
		$this->ControllerTemplate->Template = $this->Core->Url .'/engine/components/'. $this->TypeComponent .'/'. $this->ControllerName .'/templates/'. (!$this->cTemplate ? 'default' : '') .'/';
		
	}

	public function IndexAction(){
		if(!$this->Registry->Session->User['id']) return false;
		$arNavs = array(
				array(
					'icon' => 'tasks',
					'link' => '/task/',
					'name' => 'Задания'
				),
				array(
					'icon' => 'logout',
					'link' => '/logout/',
					'name' => 'Выход')
			);
		$this->ControllerTemplate->Set('login', $this->Registry->Auth->Get('login'));
		$this->ControllerTemplate->Set('arNav', $arNavs);
		$this->ControllerTemplate->Display();
	}
}