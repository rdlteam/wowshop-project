$(document).ready(function(){
	$('form').on('submit', {name : 'replayform'}, function(){
		if($('textarea[name=message]').val() < 2){
			alert('Введите сообщение');
			return false;
		}
		if($('[name=replay_user]').val() <= 0){
			alert('Ответственный не может быть пустым');
			return false;
		}
		var last_id = '';
		$.ajax({
   			type: "POST",
   			url: "/index.php?controller=engine:tasks&action=AddReplay",
   			data: $(this).serialize(),
   			dataType: 'json',
   			success: function(msg){
   				if(msg.Error == 'true') return false;
   				last_id = rand(11111, 99999);
     			$('ol.commentlist li:first').clone().appendTo('ol.commentlist').addClass('newMessage-'+ last_id);
     			$('.newMessage-'+ last_id + ' .comment-by strong').html(msg.user);
     			
     			if(msg.type == 'open'){
     				$('.newMessage-'+ last_id + ' .comment-by .reply').hide();
     			} else {
     				$('.newMessage-'+ last_id + ' .comment-by .reply').show();
     			}

     			$('.newMessage-'+ last_id + ' .comment-by .date').html(msg.date);
     			$('.newMessage-'+ last_id + ' .comment-des p').html(msg.text);
   			}
 		});

 		return false;
	});
});

function rand( min, max ) {	
	if( max ) {
		return Math.floor(Math.random() * (max - min + 1)) + min;
	} else {
		return Math.floor(Math.random() * (min + 1));
	}
}
