$(document).ready(function(){
	if(Get('count') == 'Y'){
		$('input[name=count]').attr('checked', true);
	}

	$('input[name=count]').on('click', function(){
		if($(this).is(':checked') === true){
			window.location = addParameter(window.location, 'count', 'Y');
		} else {
			window.location = addParameter(window.location, 'count', 'N');
		}
	});

	$('select[name=filter]').on('change', function(){
		window.location = addParameter(window.location, 'sort', $(this).val());
	});

    $("#myModalLogin").modal('show');
});

function addParameter(url, param, value) {
    // Using a positive lookahead (?=\=) to find the
    // given parameter, preceded by a ? or &, and followed
    // by a = with a value after than (using a non-greedy selector)
    // and then followed by a & or the end of the string
    var val = new RegExp('(\\?|\\&)' + param + '=.*?(?=(&|$))'),
        parts = url.toString().split('#'),
        url = parts[0],
        hash = parts[1]
        qstring = /\?.+$/,
        newURL = url;

    // Check if the parameter exists
    if (val.test(url))
    {
        // if it does, replace it, using the captured group
        // to determine & or ? at the beginning
        newURL = url.replace(val, '$1' + param + '=' + value);
    }
    else if (qstring.test(url))
    {
        // otherwise, if there is a query string at all
        // add the param to the end of it
        newURL = url + '&' + param + '=' + value;
    }
    else
    {
        // if there's no query string, add one
        newURL = url + '?' + param + '=' + value;
    }

    if (hash)
    {
        newURL += '#' + hash;
    }

    return newURL;
}

function Get(name){
	var $_GET = {};
	var __GET = window.location.search.substring(1).split("&");

	for(var i=0; i<__GET.length; i++) {
		var getVar = __GET[i].split("=");
		$_GET[getVar[0]] = typeof(getVar[1]) == "undefined" ? "y" : getVar[1];
	}

	return $_GET[name] || 0;
}