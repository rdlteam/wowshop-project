<?php 
// echo md5('123456');
// session_start();

ini_set('display_errors','on');
error_reporting(E_ALL);

define('SECURITY', true);
define('ROOT_DIR', __DIR__);
define('ENGINE_DIR', ROOT_DIR . '/engine/');
use Rdl\Loader\AutoLoader;
include_once ENGINE_DIR .'Core/lib/Rdl/Loader/AutoLoader.php';
$RDLClassLoader = new AutoLoader();	
$RDLClassLoader->setIncludePath(ENGINE_DIR. 'Core/lib');
$RDLClassLoader->register();

include_once(ENGINE_DIR .'Core/Core.php');

try{
	
	if(mb_strpos($_SERVER['REQUEST_URI'], 'admin/') !== false)
		RDLCore::getInstance('admin')->Run();
	else if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH'], 'UTF-8') == 'xmlhttprequest')
		RDLCore::getInstance('ajax')->Run();
	else if(!empty($_GET['thumb_file']))
		RDLCore::getInstance('thumbs')->Run();
	else
		RDLCore::getInstance('index')->Run();

} catch (\RuntimeException $e){
	die($e->getMessage());
} catch(Rdl\Exception\ErrorPage $e){
	die($e->Display());
} catch(Rdl\Exception\AjaxException $ajax){
	ob_end_clean();
	die($ajax->Display());
} catch(Rdl\Exception\HackedPage $e){
	var_dump('testsetse');

	die($e->getMessage());
	
}catch(\PDOException $e){
	die($e->getMessage());
}